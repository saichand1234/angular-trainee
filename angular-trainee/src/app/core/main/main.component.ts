import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { DialogService } from 'primeng/dynamicdialog';
import { RestApiService } from 'src/services/rest-api.service';
import { FormSectionComponent } from 'src/app/core/form-section/form-section.component'
import Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';

interface City {
  active: number,
  description: string,
  id: number,
  name: string
}
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [DialogService]
})
export class MainComponent implements OnInit {
  public datasetlist = [];
  public editData: any[];
  public dataFlowLookupArray = [];
  public dataMediumLookUpArray = [];
  public dataFileFormatLookupArray = [];
  public exportedTableLookupArray = [];
  public modalRef: BsModalRef;



  public datasetForm: FormGroup;
  public formError: boolean = false;
  constructor(public apiService: RestApiService, private _fb: FormBuilder, public datePipe: DatePipe, public dialogService: DialogService,
    public spinner: NgxSpinnerService, private modalService: BsModalService
  ) {
    this.createForm();
  }


  ngOnInit(): void {
    this.getLookupData();
    this.getInterfaceDatasetList();
  }
  public createForm() {
    this.formError = false;
    this.datasetForm = this._fb.group({
      name: new FormControl(null, Validators.required),
      effectiveFrom: new FormControl(null, [Validators.required]),
      exporteddTable: new FormControl(null, [Validators.required]),
      dataFlow: new FormControl(null, [Validators.required]),
      dataMedium: new FormControl(null, [Validators.required]),
      dataFileFormat: new FormControl(null, [Validators.required]),
      description: new FormControl(''),
      id: new FormControl(0)
    })
  }
  public getLookupData() {
    try {
      this.spinner.show();
      this.apiService.getLookupData().subscribe(res => {

        this.dataMediumLookUpArray = res.datamedium;
        this.dataFileFormatLookupArray = res.datafileformat;
        this.dataFlowLookupArray = res.dataflow;
        this.exportedTableLookupArray = [
          {
            "name": "PS",
            "id": 1,
            "value": "exported_ps_data"
          },
          {
            "name": "DCS",
            "id": 2,
            "value": "exported_dcs_data"
          },
          {
            "name": "Completed Visits",
            "id": 3,
            "value": "exported_visit_data"
          }
        ]
        this.spinner.hide()
      }, err => {
        console.log(err);
      })
    } catch (error) {
      console.log()
    }
  }
  public getInterfaceDatasetList() {
    try {
      this.spinner.show()
      this.apiService.getInterfaceDatasetList(9).subscribe(res => {
        console.log(res);
        this.datasetlist = res;
        this.spinner.hide();
      })
    } catch (error) {

    }
  }
  public editForm(id) {
    try {
      this.spinner.show();
      this.apiService.getInterfaceDataset(id).subscribe(res => {
        console.log(res)
        this.spinner.hide();
        this.datasetForm.get('name').setValue(res.name);
        this.datasetForm.get('effectiveFrom').setValue(res.effectiveDate);
        this.datasetForm.get('id').setValue(res.id);
        this.datasetForm.get('exporteddTable').setValue(res.interfaceExportedTable);
        this.datasetForm.get('description').setValue(res.description);
        this.datasetForm.get('dataMedium').setValue(res.dataMediumId);
        this.datasetForm.get('dataFlow').setValue(res.dataFlowId);
        this.datasetForm.get('dataFileFormat').setValue(res.dataFileFormatId);
      })
    } catch (error) {

    }
  }
  public save() {
    console.log(this.datasetForm.value)


    if (this.datasetForm.valid) {
      let obj = {
        active: 1,
        dataFileDelimeterId: 0,
        dataFileFormatId: this.datasetForm.value.dataFileFormat,
        dataFlowId: this.datasetForm.value.dataFlow,
        dataMediumId: this.datasetForm.value.dataMedium,
        description: this.datasetForm.value.description,
        effectiveDate: this.datePipe.transform(this.datasetForm.value.effectiveFrom, 'MM/dd/yyyy'),
        id: this.datasetForm.value.id,
        interfaceDefinitionId: 9,
        interfaceExportedTable: this.datasetForm.value.exporteddTable,
        isSyncProviderDetails: 0,
        name: this.datasetForm.value.name
      }

      try {
        this.apiService.saveInterfaceDataset(obj).subscribe(res => {
          console.log(res);
          if (res.saveFlag == 1) {
            Swal.fire("Success", 'Data Saved Succefully', 'success');
            this.getInterfaceDatasetList();
            this.createForm();
          } else {
            Swal.fire("Failed", 'Failed to Saved Data', 'error');

          }

        })
      } catch (error) {

      }



    } else {
      // alert("Invalid Form");
      Swal.fire("Invalid Form", 'Please fill all Mandatory feilds', 'error');

    }
  }

  openDialog(item) {
    console.log(item);
    this.editData = item;
    this.modalRef = this.modalService.show(FormSectionComponent, Object.assign({initialState:{data:item}}, { class: 'dataset-container'}));

  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'dataset-container' }));
  }
}
