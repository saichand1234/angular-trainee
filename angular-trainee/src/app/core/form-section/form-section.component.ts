import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { RestApiService } from 'src/services/rest-api.service';
interface City {
  active: number,
  description: string,
  id: number,
  name: string
}
@Component({
  selector: 'app-form-section',
  templateUrl: './form-section.component.html',
  styleUrls: ['./form-section.component.scss']
})
export class FormSectionComponent implements OnInit {


  public data;

  public formError: boolean = false;
  public datasetForm: FormGroup;
  public dataFlowLookupArray = [];
  public dataMediumLookUpArray = [];
  public dataFileFormatLookupArray = [];
  public exportedTableLookupArray = [];
  constructor(private _fb: FormBuilder,public datePipe: DatePipe,public apiService: RestApiService) {
    this.createForm();
   }
  ngOnInit(): void {
    this.getLookupData();
    console.log(this.data)
  }
  public createForm() {
    this.formError = false;
    this.datasetForm = this._fb.group({
      datasetId:new FormControl(''),
      name: new FormControl(null, Validators.required),
      effectiveFrom: new FormControl(null, [Validators.required]),
      exporteddTable: new FormControl(null, [Validators.required]),
      dataFlow: new FormControl(null, [Validators.required]),
      dataMedium: new FormControl(null, [Validators.required]),
      dataFileFormat: new FormControl(null, [Validators.required]),
      description: new FormControl(''),
      id: new FormControl(0)
    })
  }
  public save() {
    console.log(this.datasetForm.value)


    if(this.datasetForm.valid){
      let obj = {
        active: 1,
        dataFileDelimeterId: 0,
        dataFileFormatId: this.datasetForm.value.dataFileFormat,
        dataFlowId: this.datasetForm.value.dataFlow,
        dataMediumId: this.datasetForm.value.dataMedium,
        description: this.datasetForm.value.description,
        effectiveDate: this.datePipe.transform( this.datasetForm.value.effectiveFrom,'MM/dd/yyyy'),
        id: this.datasetForm.value.id,
        interfaceDefinitionId: 9,
        interfaceExportedTable: this.datasetForm.value.exporteddTable,
        isSyncProviderDetails: 0,
        name: this.datasetForm.value.name
      }
    }else{
      alert("Invalid Form")
    }
  }
  public getLookupData() {
    try {
      this.apiService.getLookupData().subscribe(res => {

        this.dataMediumLookUpArray = res.datamedium;
        this.dataFileFormatLookupArray = res.datafileformat;
        this.dataFlowLookupArray = res.dataflow;
        this.exportedTableLookupArray = [
          {
            "name": "PS",
            "id": 1,
            "value": "exported_ps_data"
          },
          {
            "name": "DCS",
            "id": 2,
            "value": "exported_dcs_data"
          },
          {
            "name": "Completed Visits",
            "id": 3,
            "value": "exported_visit_data"
          }
        ]

      },err=>{
        console.log(err);
      })
    } catch (error) {
console.log()
    }
  }
}
