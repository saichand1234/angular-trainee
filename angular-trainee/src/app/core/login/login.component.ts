import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from 'src/services/rest-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginform: FormGroup;
  public formError:boolean=false;

  constructor( private formBuilder: FormBuilder,private _route:Router,public apiService:RestApiService) {
     this.createForm();
   }

  ngOnInit(): void {
  }
  createForm(){
    this.formError=false;
    this.loginform= this.formBuilder.group({
    email:new FormControl (null,[Validators.required,Validators.email]),
    password:new FormControl (null,[Validators.required,Validators.pattern('')]),
  });
  }
 get loginValid(){
    return this.loginform.controls;
  }
  onSubmit(){
    this.formError=true;
    console.log(this.loginform.value)
    if(this.loginform.valid){

    }else{
      alert("Inavalid Details")
    }
  }

}
