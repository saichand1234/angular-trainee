import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { AngularService } from 'src/services/angular.service';
import { RestApiService } from 'src/services/rest-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public responseData: any;
  constructor(private http: HttpClient, private apiservice: RestApiService,private angularService:AngularService) {


  }

  ngOnInit() {
console.log("on application load")
    this.http.get("assets/url.json").subscribe(res => {
      this.responseData = res;
      sessionStorage.setItem('webserviceURL', this.responseData.webserviceURL);
      this.apiservice.webServiceUrl = this.responseData.webserviceURL;
      this.angularService.webServiceUrl=this.responseData.webserviceURL;
    })

  }
}
