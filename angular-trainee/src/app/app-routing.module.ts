import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './core/login/login.component';
import { MainComponent } from './core/main/main.component';
import { FormSectionComponent } from './core/form-section/form-section.component';

const routes: Routes = [
  {path:"login",component:LoginComponent},
  {path:"main",component:MainComponent},
  { path: 'customers', loadChildren: () => import('./core/customers/customers.module').then(m => m.CustomersModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const Myrouting=[]