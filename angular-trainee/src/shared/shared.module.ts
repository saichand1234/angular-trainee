import { NgModule } from '@angular/core';
import {InputTextModule} from 'primeng/inputtext';
import {CascadeSelectModule} from 'primeng/cascadeselect';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
//other
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


const primeng =[CardModule,InputTextModule,CascadeSelectModule,DropdownModule,InputTextareaModule,ButtonModule,DynamicDialogModule];
const shared =[BsDatepickerModule.forRoot()];
@NgModule({
  declarations: [],
  imports: [
    primeng,shared
  ],
  exports:[primeng,shared]
})
export class SharedModule { }
