import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'text/plain'
    }),

  };
  public webServiceUrl = "";

  constructor(private http: HttpClient) {
    this.webServiceUrl = sessionStorage.getItem('webserviceURL');
    console.log(this.webServiceUrl)
  }


  public getInterfaceDatasetList(id): Observable<any> {
    return this.http.get(`${this.webServiceUrl}/maintenancerestapi/getInterfaceDatasetList?jsonObject={"name":"","active":"","interfaceDefinitionId":${id}}`).pipe(catchError(this.errorHandler));
  }
  public getLookupData(): Observable<any> {
    return this.http.get(`${this.webServiceUrl}/maintenancerestapi/getLookupData?tableNames=interface_definition,data_flow,data_medium,data_file_format,data_file_delimeter,data_type,date_format`).pipe(catchError(this.errorHandler))
  }

  public saveInterfaceDataset(obj): Observable<any> {
    return this.http.post(this.webServiceUrl + '/maintenancerestapi/saveInterfaceDataset?userId=1', obj, this.httpOptions).pipe(catchError(this.errorHandler));

  }

  public getInterfaceDataset(id): Observable<any> {
    return this.http.get(`${this.webServiceUrl}/maintenancerestapi/getInterfaceDataset?id=${id}`).pipe(catchError(this.errorHandler))
  }

  private errorHandler(error: HttpErrorResponse) {
    console.log("error in EVV dashbord service", error);
    return throwError(error);
  }
}
