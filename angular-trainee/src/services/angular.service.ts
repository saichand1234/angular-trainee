import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AngularService {
public webServiceUrl="";
  constructor(private http:HttpClient) {
    this.webServiceUrl=sessionStorage.getItem('webserviceURL');
    console.log("angular service constructor",this.webServiceUrl)

   }

  public getLookupData():Observable<any>{

    return this.http.get('')

  }
}
